//
//  ViewController.swift
//  SnapTube
//
//  Created by Diego Suquillo on 8/6/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var listVideo: UITableView!
    
    var videos:[Video] = [Video]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let model = VideoModel()
        self.videos = model.getVideos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.view.frame.size.width / 320) * 180
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listVideo.dequeueReusableCell(withIdentifier: "videoCell")!
        // cell.textLabel?.text = videos[indexPath.row].videoTitle
        let videoTitle = videos[indexPath.row].videoTitle
        let label = cell.viewWithTag(2) as! UILabel
        label.text = videoTitle
        // label.text = Video
        
        let videoThumbnailUrlString = "https://i1.ytimg.com/vi/" + videos[indexPath.row].videoId + "/mqdefault.jpg"
        let url = URL(string: videoThumbnailUrlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) {
            data, response, error in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
            
            DispatchQueue.main.async {
                let imageView = cell.viewWithTag(1) as! UIImageView
                imageView.image = UIImage(data: data)
            }
        }
        
        task.resume()
        
        return cell
    }
}

