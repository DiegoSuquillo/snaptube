//
//  VideoModel.swift
//  SnapTube
//
//  Created by Diego Suquillo on 8/6/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import UIKit

class VideoModel: NSObject {
    func getVideos() -> [Video] {
        var videos = [Video]()
        
        // Create a video object
        let video1 = Video()
        let video2 = Video()
        let video3 = Video()
        let video4 = Video()
        
        // Assign properties
        video1.videoId = "ojbb6nGvIi8"
        video1.videoTitle = "How to Make an APP - Ep 1 - Toold and Materials"
        video1.videoDescription = "Test"
        
        video2.videoId = "ojbb6nGvIi8"
        video2.videoTitle = "How to Make an APP - Ep 1 - Toold and Materials"
        video2.videoDescription = "Test"
        
        video3.videoId = "ojbb6nGvIi8"
        video3.videoTitle = "How to Make an APP - Ep 1 - Toold and Materials"
        video3.videoDescription = "Test"
        
        video4.videoId = "ojbb6nGvIi8"
        video4.videoTitle = "How to Make an APP - Ep 1 - Toold and Materials"
        video4.videoDescription = "Test"
        
        // Append it into the videos arraya
        videos.append(video1)
        videos.append(video2)
        videos.append(video3)
        videos.append(video4)
        
        // Return the array to the caller
        return videos
    }
}
