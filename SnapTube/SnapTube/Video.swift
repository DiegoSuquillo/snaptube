//
//  Video.swift
//  SnapTube
//
//  Created by Diego Suquillo on 8/6/18.
//  Copyright © 2018 Diego Suquillo. All rights reserved.
//

import UIKit

class Video: NSObject {
    var videoId:String = ""
    var videoTitle:String = ""
    var videoDescription = ""
}
